<?php


namespace Astound\TopInformationBar\Block;

use Magento\Framework\Api\SortOrderBuilder;
use Magento\Framework\Data\Collection;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Astound\TopInformationBar\Model\TopInformationBarRepository;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Astound\TopInformationBar\Api\Data\TopInformationBarInterface;

class View extends Template
{
    /**
     * @var TopInformationBarRepository
     */
    protected $informationBarRepository;
    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;
    /**
     * @var SortOrderBuilder
     */
    private $sortOrderBuilder;

    /**
     * View constructor.
     * @param TopInformationBarRepository $informationBarRepository
     * @param StoreManagerInterface $storeManager
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param SortOrderBuilder $sortOrderBuilder
     * @param Context $context
     * @param array $data
     */
    public function __construct(
        TopInformationBarRepository $informationBarRepository,
        StoreManagerInterface $storeManager,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        SortOrderBuilder $sortOrderBuilder,
        Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->informationBarRepository = $informationBarRepository;
        $this->storeManager = $storeManager;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->sortOrderBuilder = $sortOrderBuilder;
    }

    /**
     * @return mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getTopBars()
    {
        $currentStore =$this->storeManager->getStore()->getId();
        $prioritySort = $this->sortOrderBuilder
            ->setField('priority')
            ->setDirection(Collection::SORT_ORDER_ASC)
            ->create();
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter('status',TopInformationBarInterface::STATUS_ENABLE,'eq')
            ->addFilter('store_id', $currentStore, 'eq')
            ->addSortOrder($prioritySort)
            ->create();
        $informationBars = $this->informationBarRepository->getList($searchCriteria);
        return $informationBars;
    }
}
