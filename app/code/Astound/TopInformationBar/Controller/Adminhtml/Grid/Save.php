<?php

namespace Astound\TopInformationBar\Controller\Adminhtml\Grid;

use Magento\Backend\App\Action;
use Astound\TopInformationBar\Model\TopInformationBarRepository;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Astound\TopInformationBar\Model\TopInformationBarFactory;

class Save extends Action
{
    /**
     * @var TopInformationBarRepository
     */
    protected $modelRepository;

    /**
     * @var TopInformationBarFactory
     */
    protected $modelFactory;

    /**
     * Save constructor.
     * @param Action\Context $context
     * @param TopInformationBarRepository $modelRepository
     * @param TopInformationBarFactory $modelFactory
     */
    public function __construct(
        Action\Context $context,
        TopInformationBarRepository $modelRepository,
        TopInformationBarFactory $modelFactory
    ) {
        parent::__construct($context);
        $this->modelRepository = $modelRepository;
        $this->modelFactory = $modelFactory;
    }


    /**
     * @return ResponseInterface|Redirect|ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            $id = $this->getRequest()->getParam('entity_id');
            if ($id) {
                try {
                    $topBar = $this->modelRepository->get($id);
                } catch (\Exception $e) {
                    $this->messageManager->addErrorMessage(__('This bar no longer exists.'));
                    return $resultRedirect->setPath('*/*/');
                }

            } else {
                $topBar = $this->modelFactory->create();
            }

            $topBar->setData($data);

            try {
                $this->modelRepository->save($topBar);
                $this->messageManager->addSuccessMessage(__('Top Bar saved'));
                $this->_getSession()->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['id' => $topBar->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\RuntimeException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e, __('Something went wrong while saving the department'));
            }

            $this->_getSession()->setFormData($data);
            return $resultRedirect->setPath('*/*/edit', ['entity_id' => $this->getRequest()->getParam('id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}