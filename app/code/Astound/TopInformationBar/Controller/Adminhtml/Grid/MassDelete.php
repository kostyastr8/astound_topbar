<?php

namespace Astound\TopInformationBar\Controller\Adminhtml\Grid;

use Astound\TopInformationBar\Model\TopInformationBarRepository;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Ui\Component\MassAction\Filter;
use Astound\TopInformationBar\Model\ResourceModel\TopInformationBar\TopBarCollectionFactory;

class MassDelete extends Action
{

    /**
     * @var Filter
     */
    protected $filter;

    /**
     * @var TopBarCollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var TopInformationBarRepository
     */
    private $infoBarRepository;

    /**
     * MassDelete constructor.
     * @param Context $context
     * @param Filter $filter
     * @param TopBarCollectionFactory $collectionFactory
     * @param TopInformationBarRepository $infoBarRepository
     */
    public function __construct(
        Context $context,
        Filter $filter,
        TopBarCollectionFactory $collectionFactory,
        TopInformationBarRepository $infoBarRepository
    ) {
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        parent::__construct($context);
        $this->infoBarRepository = $infoBarRepository;
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     * @throws NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute()
    {
        $collection = $this->filter->getCollection($this->collectionFactory->create());
        $collectionSize = $collection->getSize();

        foreach ($collection as $item) {
            try {
                $this->infoBarRepository->deleteById($item->getId());
            } catch (\Exception $e) {
                throw new NoSuchEntityException(__($e->getMessage()));
            }
        }

        $this->messageManager->addSuccessMessage(__('A total of %1 record(s) have been deleted.', $collectionSize));

        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath($this->_redirect->getRefererUrl());
    }
}