<?php

namespace Astound\TopInformationBar\Controller\Adminhtml\Grid;

use Astound\TopInformationBar\Model\TopInformationBarRepository;
use Magento\Backend\App\Action;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\NoSuchEntityException;

class Delete extends Action
{

    /**
     * @var TopInformationBarRepository
     */
    private $modelRepository;

    /**
     * Delete constructor.
     * @param Action\Context $context
     * @param TopInformationBarRepository $modelRepository
     */
    public function __construct(
        Action\Context $context,
        TopInformationBarRepository $modelRepository
    ) {
        parent::__construct($context);
        $this->modelRepository = $modelRepository;
    }


    /**
     * @return ResponseInterface|Redirect|ResultInterface
     * @throws NoSuchEntityException
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        $topBar= $this->modelRepository->get($id);
        if (!$topBar) {
            $this->messageManager->addErrorMessage(__('Unable to proceed. Please, try again.'));
            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('*/*/index', ['current' => true]);
        }
        try {
            $this->modelRepository->deleteById($id);
            $this->messageManager->addSuccessMessage(__('Your top bar has been deleted !'));
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__('Error while trying to delete contact: '));
            $resultRedirect = $this->resultRedirectFactory->create();
            return $resultRedirect->setPath('*/*/index', ['current' => true]);
        }

        $resultRedirect = $this->resultRedirectFactory->create();
        return $resultRedirect->setPath('*/*/index', ['current' => true]);
    }
}