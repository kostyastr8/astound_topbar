<?php

namespace Astound\TopInformationBar\Controller\Adminhtml\Grid;

use Astound\TopInformationBar\Model\TopInformationBarRepository;
use Astound\TopInformationBar\Model\TopInformationBarFactory;
use Magento\Backend\App\Action;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\Page;
use Magento\Framework\View\Result\PageFactory;
use Magento\Setup\Exception;

class Edit extends Action
{
    /**
     * @var Registry|null
     */
    protected $coreRegistry;

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var TopInformationBarRepository
     */
    protected $modelRepository;

    /**
     * @var TopInformationBarFactory
     */
    protected $modelFactory;

    /**
     * Edit constructor.
     * @param Action\Context $context
     * @param PageFactory $resultPageFactory
     * @param Registry $registry
     * @param TopInformationBarRepository $modelRepository
     * @param TopInformationBarFactory $modelFactory
     */
    public function __construct(
        Action\Context $context,
        PageFactory $resultPageFactory,
        Registry $registry,
        TopInformationBarRepository $modelRepository,
        TopInformationBarFactory $modelFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->coreRegistry = $registry;
        parent::__construct($context);
        $this->modelRepository = $modelRepository;
        $this->modelFactory = $modelFactory;
    }

    /**
     * @return Page
     */
    protected function _initAction()
    {
        // load layout, set active menu and breadcrumbs
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Astound_TopInformationBar::topinformationbar')
            ->addBreadcrumb(__('Top Information Bar'), __('Top Information Bar'))
            ->addBreadcrumb(__('Manage Top Information Bar'), __('Manage Top Information Bar'));
        return $resultPage;
    }

    /**
     * @return ResponseInterface|Redirect|ResultInterface|Page
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        $topBar =  $this->modelFactory->create();
        if ($id) {
            try {
                $topBar = $this->modelRepository->get($id);
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage(__('This Top Information Bar not exists.'));
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        }

        $data = $this->_getSession()->getFormData(true);
        if (!empty($data)) {
            $topBar->setData($data);
        }

        $this->coreRegistry->register('topinformationbar', $topBar);

        $resultPage = $this->_initAction();
        $resultPage->addBreadcrumb(
            $id ? __('Edit Top Information Bar') : __('New Top Information Bar'),
            $id ? __('Edit Top Information Bar') : __('NewTop Information Bar')
        );
        $resultPage->getConfig()->getTitle()->prepend(__('Top Information Bars'));
        $resultPage->getConfig()->getTitle()
            ->prepend($topBar->getId() ? $topBar->getTitle() : __('New Top Information Bar'));

        return $resultPage;
    }
}