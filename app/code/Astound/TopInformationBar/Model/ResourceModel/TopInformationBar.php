<?php

namespace Astound\TopInformationBar\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;

class TopInformationBar extends  AbstractDb
{
    /**
     * TopInformationBar constructor.
     * @param Context $context
     */
    public function __construct(
        Context $context
    ) {
        parent::__construct($context);
    }


    protected function _construct()
    {
        $this->_init('astound_infobar', 'entity_id');
    }
}