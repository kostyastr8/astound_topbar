<?php

namespace Astound\TopInformationBar\Model\ResourceModel\TopInformationBar;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Astound\TopInformationBar\Model\TopInformationBar;
use Astound\TopInformationBar\Model\ResourceModel\TopInformationBar as ResourceModel;

class TopBarCollection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $idFieldName = 'entity_id';

    protected function construct()
    {
        $this->_init(
            TopInformationBar::class,
            ResourceModel::class
        );
    }
}