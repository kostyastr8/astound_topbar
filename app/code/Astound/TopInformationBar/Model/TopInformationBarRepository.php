<?php

namespace Astound\TopInformationBar\Model;

use Astound\TopInformationBar\Api\Data\TopInformationBarInterface;
use Astound\TopInformationBar\Api\Data\TopInformationBarSearchResultsInterface;
use Astound\TopInformationBar\Api\TopInformationBarRepositoryInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchResults;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Astound\TopInformationBar\Model\ResourceModel\TopInformationBar as ResourceModel;
use Astound\TopInformationBar\Model\ResourceModel\TopInformationBar\TopBarCollectionFactory;
use Astound\TopInformationBar\Api\Data\TopInformationBarInterfaceFactory;
use Astound\TopInformationBar\Api\Data\TopInformationBarSearchResultsInterfaceFactory;
use Magento\Framework\Api\SearchCriteriaInterface;

class TopInformationBarRepository implements TopInformationBarRepositoryInterface
{

    /**
     * @var ResourceModel
     */
    protected $resource;

    /**
     * @var CollectionProcessorInterface
     */
    protected $collectionProcessor;

    /**
     * @var TopBarCollectionFactory
     */
    private $topBarCollection;

    /**
     * @var TopInformationBarInterfaceFactory
     */
    private $searchResultsFactory;

    /**
     * @var TopInformationBarSearchResultsInterfaceFactory
     */
    private $topBarFactory;


    /**
     * TopInformationBarRepository constructor.
     * @param ResourceModel $resource
     * @param TopBarCollectionFactory $topBarCollection
     * @param CollectionProcessorInterface $collectionProcessor
     * @param TopInformationBarInterfaceFactory $topBarFactory
     * @param TopInformationBarSearchResultsInterfaceFactory $searchResultsFactory
     */
    public function __construct(
        ResourceModel $resource,
        TopBarCollectionFactory $topBarCollection,
        CollectionProcessorInterface $collectionProcessor,
        TopInformationBarInterfaceFactory $topBarFactory,
        TopInformationBarSearchResultsInterfaceFactory $searchResultsFactory
    ) {
        $this->resource = $resource;
        $this->topBarCollection = $topBarCollection;
        $this->collectionProcessor = $collectionProcessor;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->topBarFactory = $topBarFactory;
    }

    /**
     * @param $topBar
     * @return mixed
     * @throws CouldNotSaveException
     */
    public function save($topBar)
    {
        try {
            $this->resource->save($topBar);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }
        return $topBar;
    }

    /**
     * @param $topBarId
     * @return mixed
     * @throws NoSuchEntityException
     */
    public function get($topBarId)
    {
        $topBar = $this->topBarFactory->create();
        $this->resource->load($topBar, $topBarId);
        if (!$topBar->getId()) {
            throw new NoSuchEntityException(__('Top bar entity with id `%1` does not exist.', $topBarId));
        }
        return $topBar;
    }


    /**
     * @param TopInformationBarInterface $topBar
     * @return bool|mixed
     * @throws CouldNotDeleteException
     */
    public function delete(TopInformationBarInterface $topBar)
    {
        try {
            $this->resource->delete($topBar);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        return true;
    }

    /**
     * @param $topBarId
     * @return bool|mixed
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     */
    public function deleteById($topBarId)
    {
        return $this->delete($this->get($topBarId));
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return mixed
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        /** @var Astound\TopInformationBar\Model\ResourceModel\TopInformationBar\TopBarCollectionFactory $collection */
        $collection = $this->topBarCollection->create();
        $this->collectionProcessor->process($searchCriteria, $collection);

        /** @var SearchResults $searchResults */
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount(count($collection->getItems()));
        return $searchResults;
    }
}