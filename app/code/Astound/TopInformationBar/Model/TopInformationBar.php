<?php

namespace Astound\TopInformationBar\Model;

use Astound\TopInformationBar\Api\Data\TopInformationBarInterface;
use Magento\Framework\Model\AbstractModel;
use Astound\TopInformationBar\Model\ResourceModel\TopInformationBar as ResourceTopInformationBar;


class TopInformationBar extends AbstractModel implements TopInformationBarInterface
{


    protected function _construct()
    {
        $this->_init(ResourceTopInformationBar::class);
    }

    /**
     * @return mixed|string
     */
    public function getTitle()
    {
        return $this->getData(self::TITLE);
    }

    /**
     * @param $title
     * @return TopInformationBar|string
     */
    public function setTitle($title)
    {
        return $this->setData(self::TITLE, $title);
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->getData(self::CONTENT);
    }

    /***
     * @param $content
     * @return TopInformationBar|string
     */
    public function setContent($content)
    {
        return $this->setData(self::CONTENT, $content);
    }

    /**
     * @return mixed|string
     */
    public function getBackgroundColor()
    {
        return $this->getData(self::BACKGROUND_COLOR);
    }

    /**
     * @param $backgroundColor
     * @return TopInformationBar|string
     */
    public function setBackgroundColor($backgroundColor)
    {
        return $this->setData(self::BACKGROUND_COLOR, $backgroundColor);
    }

    /**
     * @return int|mixed
     */
    public function getStoreId()
    {
        return $this->getData(self::STORE_ID);
    }

    /**
     * @param $storeId
     * @return TopInformationBar|int
     */
    public function setStoreId($storeId)
    {
        return $this->setData(self::STORE_ID, $storeId);
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->getData(self::STATUS);
    }

    /**
     * @param $status
     * @return TopInformationBar|int
     */
    public function setStatus($status)
    {
        return $this->setData(self::STATUS, $status);
    }


    public function getAvailableStatuses()
    {
        return [self::STATUS_ENABLE => __('Enabled'), self::STATUS_DISABLE => __('Disabled')];
    }

    /**
     * @return int|mixed
     */
    public function getPriority()
    {
        return $this->getData(self::PRIORITY);
    }

    /**
     * @param $priority
     * @return mixed
     */
    public function setPriority($priority)
    {
        return $this->setData(self::PRIORITY, $priority);

    }


}