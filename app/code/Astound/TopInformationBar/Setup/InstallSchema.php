<?php

namespace Astound\TopInformationBar\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        $table = $installer->getConnection()
            ->newTable($installer->getTable('astound_infobar'))
            ->addColumn(
               'entity_id',
               Table::TYPE_INTEGER,
               null,
               ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
               'Entity Id'
            )->addColumn(
                'title',
                Table::TYPE_TEXT,
                255,
                ['nullable' => false, 'default' => NULL],
                'Name of the bar'
            )->addColumn(
                'content',
                Table::TYPE_TEXT,
                2048,
                [],
                'Content of the Info bar'
            )->addColumn(
                'background_color',
                Table::TYPE_TEXT,
                7,
                ['nullable' => false, 'default' => '#ffffff'],
                'Color in #XXXXXX format to set Info bar background'
            )->addColumn(
                'store_id',
                Table::TYPE_SMALLINT,
                null,
                ['nullable' => false, 'default' => 1],
                'List of available and applicable stores views'
            )->addColumn(
                'status',
                Table::TYPE_BOOLEAN,
                null,
                ['nullable' => false, 'default' => TRUE],
                'EnabledDisabled'
            )->addColumn(
                'priority',
                Table::TYPE_INTEGER,
                null,
                ['nullable' => true, 'default' => 0],
                'Priority to display on frontend (ASC).'
            );

        $table->setComment('Astound Top Information Bar');

        $installer->getConnection()->createTable($table);




        $installer->endSetup();
    }

}
