<?php

namespace Astound\TopInformationBar\Api;

use Astound\TopInformationBar\Api\Data\TopInformationBarInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

interface TopInformationBarRepositoryInterface
{

    /**
     * @param $topBar
     * @return mixed
     */
    public function save($topBar);

    /**
     * @param integer $topBarId
     * @return mixed
     */
    public function get($topBarId);

    /**
     * @param TopInformationBarInterface $topBar
     * @return mixed
     */
    public function delete(TopInformationBarInterface $topBar);

    /**
     * @param $countryId
     * @return mixed
     */
    public function deleteById($countryId);

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return mixed
     */
    public function getList(SearchCriteriaInterface $searchCriteria);
}