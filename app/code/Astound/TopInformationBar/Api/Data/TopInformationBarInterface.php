<?php

namespace Astound\TopInformationBar\Api\Data;

interface TopInformationBarInterface
{
    const ID = 'entity_id';
    const TITLE = 'title';
    const CONTENT = 'content';
    const BACKGROUND_COLOR = 'background_color';
    const STORE_ID = 'store_id';
    const STATUS = 'available';
    const PRIORITY = 'priority';

    const STATUS_DISABLE= 0;
    const STATUS_ENABLE = 1;

    /**
     * @return integer
     */
    public function getId();

    /**
     * @param $id
     * @return integer
     */
    public function setId($id);

    /**
     * @return string
     */
    public function getTitle();

    /**
     * @param $title
     * @return string
     */
    public function setTitle($title);

    /**
     * @return string
     */
    public function getContent();

    /**
     * @param $content
     * @return string
     */
    public function setContent($content);

    /**
     * @return string
     */
    public function getBackgroundColor();

    /**
     * @param $backgroundColor
     * @return string
     */
    public function setBackgroundColor($backgroundColor);

    /**
     * @return integer
     */
    public function getStoreId();

    /**
     * @param $storeId
     * @return integer
     */
    public function setStoreId($storeId);

    /**
     * @return integer
     */
    public function getStatus();

    /**
     * @param $status
     * @return integer
     */
    public function setStatus($status);

    /**
     * @return integer
     */
    public function getPriority();

    /**
     * @param $priority
     * @return integer
     */
    public function setPriority($priority);
}